<?php
namespace Admin\Model;
use Think\Model;

class NovelModel extends Model {
    public function novel_list($where = array()) {
        $list = $this->where($where)->order('created_time desc')->select();
        return $list;
    }

    public function delete_novel($ids) {
        $this->startTrans();
        if (is_array($ids)) {
            $result = (($this->where(array('id' => array('in', $ids)))->delete()) === false ? fasle : true);
            if ($result) {
                $this->where(array('id' => array('in', $ids)))->delete();
                $this->commit();
            } else {
                $this->rollback();
            }
            return $result;
        } else {
            $result = ($this->where(array('id' => $ids))->delete()) === false ? fasle : true;
            if ($result) {
                $this->where(array('id' => $ids))->delete();
                $this->commit();
            } else {
                $this->rollback();
            }
            return $result;
        }
    }
}
