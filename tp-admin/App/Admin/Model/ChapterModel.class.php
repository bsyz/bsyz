<?php
namespace Admin\Model;
use Think\Model;

class ChapterModel extends Model {
    public function delete_chapter($ids) {
        $this->startTrans();
        if (is_array($ids)) {
            $result = (($this->where(array('id' => array('in', $ids)))->delete()) === false ? fasle : true);
            if ($result) {
                $this->where(array('id' => array('in', $ids)))->delete();
                $this->commit();
            } else {
                $this->rollback();
            }
            return $result;
        } else {
            $result = ($this->where(array('id' => $ids))->delete()) === false ? fasle : true;
            if ($result) {
                $this->where(array('id' => $ids))->delete();
                $this->commit();
            } else {
                $this->rollback();
            }
            return $result;
        }
    }
}
