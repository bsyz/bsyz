<?php
namespace Admin\Model;
use Think\Model;

class SubjectModel extends Model {
	public function __construct() {
		parent::__construct('novel_subject');
	}
    public function subject_list($where = array()) {
        $list = $this->where($where)->order('created_time desc')->select();
        return $list;
    }

    public function delete_subject($ids) {
        $this->startTrans();
        if (is_array($ids)) {
            $result = (($this->where(array('id' => array('in', $ids)))->delete()) === false ? fasle : true);
            if ($result) {
                $this->where(array('id' => array('in', $ids)))->delete();
                $this->commit();
            } else {
                $this->rollback();
            }
            return $result;
        } else {
            $result = ($this->where(array('id' => $ids))->delete()) === false ? fasle : true;
            if ($result) {
                $this->where(array('id' => $ids))->delete();
                $this->commit();
            } else {
                $this->rollback();
            }
            return $result;
        }
    }
}
