<?php
namespace Admin\Controller;
use Admin\Controller\CommonController;

/**
 * 小说
*/
class ChapterController extends CommonController {
    protected $db;
    function __construct() {
        parent::__construct();
        $this->db = D("Chapter");
    }

    public function index() {
        $nid = $_GET['nid'];
        $chapters = $this->db->where(array('nid'=>$nid))->select();
        $this->assign('chapters',$chapters);
        $this->display();
    }

    public function delete() {
        if (IS_POST) {
            $ids = $_POST['ids'];
            if (!empty($ids) && is_array($ids)) {
                if ($this->db->delete_chapter($ids)) {
                    $this->success('删除成功！');
                } else {
                    $this->error('删除失败！');
                }
            } else {
                $this->error("您没有勾选信息");
            }
        } else {
            if ($this->db->delete_chapter(intval($_GET['id']))) {
                $this->success('删除成功！');
            } else {
                $this->error('删除失败！');
            }
        }
    }

    public function content(){
        $id = $_GET['id'];
        $chapter = $this->db->where(array('id'=>$id))->find();
        $filename = C('NOVEL_PATH').$chapter['content'];
        $content = file_get_contents($filename);
        $content = iconv('gbk', 'utf-8', $content);

        $this->assign('content', $content);
        $this->assign('chapter', $chapter);
        $this->display();
    }

    public function edit(){
        $id = $_POST['id'];

    }
}