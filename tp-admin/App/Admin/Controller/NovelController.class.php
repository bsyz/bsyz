<?php
namespace Admin\Controller;
use Admin\Controller\CommonController;

/**
 * 小说
*/
class NovelController extends CommonController {
    protected $db;
    function __construct() {
        parent::__construct();
        $this->db = D("Novel");
    }

    public function index() {
        $novels = $this->db->novel_list();
        foreach($novels as $key=>$val){
            $novels[$key]['chapter_num'] = D('Chapter')->where(array('nid'=>$val['id']))->count();
        }
        $this->assign('novels',$novels);
        $this->display();
    }

    public function delete() {
        if (IS_POST) {
            $ids = $_POST['ids'];
            if (!empty($ids) && is_array($ids)) {
                if ($this->db->delete_novel($ids)) {
                    $this->success('删除成功！');
                } else {
                    $this->error('删除失败！');
                }
            } else {
                $this->error("您没有勾选信息");
            }
        } else {
            if ($this->db->delete_novel(intval($_GET['id']))) {
                $this->success('删除成功！');
            } else {
                $this->error('删除失败！');
            }
          }
        }
}