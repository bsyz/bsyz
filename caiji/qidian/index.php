<?php
include '../header.php';

$max_page = 2000;
$base_url = 'http://all.qidian.com/Book/BookStore.aspx?Action=5&vip=0&PageIndex=';


//分页
for($i=0; $i<$max_page; $i++){
	$url = $base_url . $i;
	$html = getHtml($url);
	//抓取列表页面信息
	$html = substr($html, strpos($html, 'storelistbt3e'));
	$html = substr($html, 0, strpos($html, 'storelistbottom'));

	preg_match_all('/class\=\"swa\">(.*)<\/div>/Usi', $html, $mcates);
	$cates = array();
	foreach($mcates[1] as $key=>$cate){
		$text = strip_tags($cate);
		$cates[$key] = trim(substr($text, 1, strpos($text, '/')-1));
	}
	//vdump($cates);
	foreach($cates as $cate){
		$res = $mysql->field('count(*) as count')
			->where(array('category'=>"'$cate'"))
			->select($dbConfig['pre'] . 'cate');
		if($res[0]['count']==0){
			$data = array('category'=>$cate);
			$mysql->insert($dbConfig['pre'] . 'cate',$data);
		}
	}
	/*
	preg_match_all('/class\=\"swc\">(.*)<\/div>/Usi', $html, $mword_counts);
	$word_counts = array();
	foreach($mword_counts[1] as $key=>$word_count){
		$text = strip_tags($word_count);
		$word_counts[$key] = trim($text);
	}
	*/
	//vdump($word_counts);

	preg_match_all('/class\=\"swd\">(.*)<\/div>/Usi', $html, $mauthors);
	$authors = array();
	foreach($mauthors[1] as $key=>$author){
		$text = trim(strip_tags($author));
		$authors[$key] = trim($text, '...');
	}
	//vdump($authors);

	preg_match_all('/class\=\"swb\">(.*)<\/div>/Usi', $html, $mnovels);
	$novels = array();
	foreach($mnovels[1] as $key=>$novel){
		preg_match('/class\=\"swbt\">(.*)<\/span>/Usi', $novel, $mnovel);
		$novels[$key] = trim($mnovel[1]);
	}
	foreach($novels as $novel){
		$novelname = trim(strip_tags($novel));
		preg_match('/href=\"([^"]*)\"/', $novel, $m);
		$url = $m[1];

		$res = $mysql->field(array('id'))
			->where(array('name'=>"'$novelname'"))
			->get_one($dbConfig['pre'] . 'novel');
		if(empty($res)){
			$data = array(
				'name'=>$novelname,
				'category'=>$cates[$key],
				'author' => $authors[$key],
			);
			$mysql->insert($dbConfig['pre'] . 'novel',$data);
			$id = $mysql->lastid();
		} else {
			$id = $res['id'];
		}
		//echo $url;exit;
		getList($url, $id);
	}
	//vdump($novels);


	//详情页面信息
	echo $html; exit;


}


function getList($url, $nid){
	global $mysql, $dbConfig;
	//小说详情页面
	$html = getHtml($url);

	preg_match('/<span itemprop=\"description\">([^<]*)<\/span>/isU', $html, $m);
	$description = $m[1];

	preg_match('/<img itemprop="image" src=\"([^"]*)\"/isU', $html, $m);
	$imgurl = $m[1];

	$html = substr($html, strpos($html, '点击阅读'), 255);
	$html = substr($html, strpos($html, 0, '点击阅读'));
	$url = preg_match_url($html);

	//更新小说信息
	$data = array(
		'des' => $description,
		'noveloriimg' => $imgurl,
	);
	$mysql->where(array('id'=>$nid))->update($dbConfig['pre'] . 'novel',$data);
	
	//echo $url;
	//小说章回页面
	$html = getHtml($url);
	//抓取列表页面信息
	$html = substr($html, strpos($html, 'id="content"'));
	$html = substr($html, 0, strpos($html, 'class="book_opt"'));

	preg_match_all('/<\/a>(.*)<\/b>/Usi', $html, $m);
	$titles = $m[1];


	//卷
	foreach($titles as $title){
		preg_match_all('/<ul>(.*)<\/ul>/Usi', $html, $m);
		$parts = $m[1];
		foreach($parts as $part){
			preg_match_all('/<li [^>]*>(.*)<\/li>/Usi', $part, $m);
			$lists = $m[1];
			$index = 1;
			foreach($lists as $list){
				$c_title = strip_tags($list);

				preg_match('/href=\"([^"]*)\"/Usi', $list, $m);
				$c_url = $m[1];
				//echo $c_url;exit;

				$subject = trim(strip_tags($list));
				//echo $subject;exit;
				if(!$subject) continue;

				$res = $mysql->field(array('id'))
					->where(array('name'=>"'$subject'", 'nid'=>$nid))
					->limit(1,1)
					->get_one($dbConfig['pre'] . 'chapter');
				if(empty($res)){
					$data = array(
						'nid' => $nid,
						'name' => $subject,
					);
					$mysql->insert($dbConfig['pre'] . 'chapter',$data);
					$chapter_id = $mysql->lastid();
				} else {
					$chapter_id = $res['id'];
				}
				
				//存小说
				getNovel($c_url, $index, $nid, $chapter_id);
				$index ++;
			}
		}
	}
	
	//echo $html;exit;
}

function getNovel($url, $name, $nid, $chapter_id){
	global $mysql, $dbConfig;
	//存小说
	$base_dir = '../novel/';
	$store_dir = date('Y').'/'.date('m').'/'.date('d');
	$dir = $base_dir . $store_dir;
	if(!is_dir($dir)){
		mkdir($dir, 0777, ture);
	}
	$filename = $dir.'/'.$name.'.txt';
	$html = getHtml($url);
	preg_match('/<div class="bookreadercontent" id="chaptercontent">(.*)<\/div>/Usi', $html, $m);
	$js_html = $m[1];
	if(preg_match("/<script src='([^\']*)'/", $js_html, $m)){
		$content = file_get_contents($m[1]);
		$content = trim(substr($content, strpos($content, "document.write('")+16));
		$content = trim($content, "');");
		$content = trim($content);
		
		file_put_contents($filename, $content);

		$data = array(
			'content' => $store_dir.'/'.$name.'.txt',
		);
		
		$mysql->where(array('id'=>$chapter_id))->update($dbConfig['pre'] . 'chapter',$data);
	}
}

include '../footer.php';